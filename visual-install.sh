#!/bin/bash

##########################################################
#                                                        #
#   File: Visual Installer (Docker)                      #
#   Type: BASH Script                                    #
#   Target OS: Ubuntu 20.04 LTS (Debian)                 #
#   Author: Truc Minh Phan <truc@trucphan.com>           #
#   Description:                                         #
#                                                        #
#   This script is used to visually install docker       #
#   and laradock to current Ubuntu install		         #
#                                                        #
#   NOTE: Requires `su` / `sudo` access to run.          #
#                                                        #
##########################################################

# Include: Bash Helpers File
source ./bash-helpers/bash-helpers.sh


# Define: Global Script Variables
scriptVersion="v1.0.0-alpha";
scriptName="Visual Laradock Installer: $scriptVersion";
scriptAuthor="Truc Minh Phan <truc@trucphan.com>";
scriptTitle="$scriptName | $scriptAuthor";
currentUser=$SUDO_USER



# -------- +++ Runtime: Program Output +++ -------- #
echo " ";
echo "###################################";
echo "#                                 #"
echo "#    Visual Laradock Installer    #";
echo "#                                 #"
echo "###################################";
echo " ";

welcomeDialog=$(whiptail --backtitle "$scriptTitle" --title "Welcome" --msgbox "\nWelcome to the Visual Laradock installer. \
Follow the prompts to begin installation of Laradock onto your machine.\n\n\
Please run this BASH Script with \`sudo\` privileges.\n\nAuthor: $scriptAuthor\
\nLicense: MIT License" 15 78  3>&1 1>&2 2>&3);

# -------- +++ /Runtime: Program Output +++ -------- #

prettyOutput 'Beginning installation of Laradock...';


# -------------------- +++ Runtime: Check User Privileges +++ ------------------ #
if (( $EUID != 0 )); then
    whiptail --backtitle "$scriptName" --title "ERROR: Missing Privileges" --msgbox "\nVisual Laradock installer requires \
super/sudo privileges to run. Please run the script using the following command:\n\n\n\
                    sudo ./visual-install.sh\n\n" 13 78
    errorMessage "ERROR: \`sudo\` privileges required. Please run as root.";
    exit;
fi
# ------------------- +++ /Runtime: Check User Privileges +++ ------------------ #


# ------------------- +++ Git: Track Changes +++ ------------------ #
# Define: Update repositories for re-forking
updateFork=$(whiptail --backtitle "$scriptTitle" --title "Git: Track changes (lionslair)" --yesno "Would you like to track changes for this repository based off lionslair repository?\n\n\n    https://bitbucket.org/lionslair/laradock.git" 11 78 --defaultno  3>&1 1>&2 2>&3);

if [ updateFork = 1 ]; then
	prettyOutput 'Adding upstream remote "https://bitbucket.org/lionslair/laradock.git" to current repository...';

	if [ $( git remote -v | grep -c "https://bitbucket.org/lionslair/laradock.git" ) -eq 0 ]; then
		git remote add upstream https://bitbucket.org/lionslair/laradock.git
		prettyOutput 'Fetching upstream from git';
		git fetch upstream
	else
		warningMessage 'Upstream remote "https://bitbucket.org/lionslair/laradock.git" already added. Skipping...';
	fi
else
	prettyOutput 'Skipping adding upstream remote...';
fi
# ------------------- +++ /Git: Track Changes +++ ------------------ #


# ------------------- +++ OS: Set correct directory +++ ------------------ #
currentLaradockDir=$PWD
prettyOutput "Current Directory is: $currentLaradockDir";
targetLaradockDir=$(whiptail --backtitle "$scriptTitle" --title "Set Laradock directory" --inputbox "Type in full directory where laradock was cloned below:" 8 78 "$currentLaradockDir"  3>&1 1>&2 2>&3);

if [ currentLaradockDir != targetLaradockDir ]; then
	warningMessage "Laradock directory changed to: $targetLaradockDir";
fi
# ------------------- +++ /OS: Set correct directory +++ ------------------ #


# ------------------- +++ OS: Update & Install Packages +++ ------------------ #
proceedInstall=$(whiptail --backtitle "$scriptTitle" --title "Proceed with installation" --yesno "Would you like to proceed with Laradock installation?" 8 78 3>&1 1>&2 2>&3);


## Sanity-Check: Ensure proceed with install
if [ $? -ne 0 ]; then
    warningMessage "WARNING: User cancelled Laradock installation. Exiting.";
    exit;
else 

	installCommands=()

	if [ $( dpkg-query -W -f='${Status}' docker-ce 2>/dev/null | grep -c "ok installed" ) -eq 0 ]; then
		installCommands+=("apt-get update")
		installCommands+=("apt-get install -qy apt-transport-https ca-certificates curl gnupg-agent software-properties-common")
		installCommands+=("curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add")
		installCommands+=('sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
		installCommands+=("apt-get update")
		installCommands+=("apt-get install -qy docker-ce docker-ce-cli containerd.io docker-compose")

		for command in "${installCommands[@]}"
		do :
			eval "${command} &"
			warningMessage "Running command ${command}"
			wait
		done

	else
		prettyOutput "Docker already installed...";

	fi

	if [ ! $( getent group docker ) ]; then
		totalCommands=$((${#installCommands[@]}+2));
	else
		totalCommands=$((${#installCommands[@]}+1));
	fi


	if [ ! $( getent group docker ) ]; then
		prettyOutput "Adding group docker to system..."
		groupadd docker
		usermod -a -G docker $currentUser
	else
		prettyOutput "System already has group \`docker\`. Skipping..."
	fi


	sleep 2s
	if [ $(dpkg-query -W -f='${Status}' docker-ce 2>/dev/null | grep -c "ok installed") -eq 1 ]; then
		if [ $( systemctl list-unit-files | grep "docker.service" | grep -c "masked\|disabled" ) -ne 0 ]; then
			prettyOutput "Beginning systemctl commands..."
			sleep 5s
			systemctl restart docker
			prettyOutput "Docker service restarted..."
			sleep 1s
			systemctl enable docker
			prettyOutput "Docker service enabled..."
		else
			prettyServiceOutput "$( systemctl list-unit-files | grep 'docker.service' )"
			warningMessage "Docker installed, but not detected as disabled. Skipping..."
		fi
	else
		warningMessage "Docker-ce package could not be detected as installed!"
	fi

fi


# ------------------- +++ /OS: Update & Install Packages +++ ------------------ #


# ------------------- +++ Docker: Select Services +++ ------------------ #
supportedDockerImages=()
supportedDockerImages+=("apache2" "apache2 Web-Server)" OFF)
supportedDockerImages+=("caddy" "Caddy Web-Server (apache2 alternative)" OFF)
supportedDockerImages+=("laravel-horizon" "Laravel Horizon" OFF)
supportedDockerImages+=("laravel-echo-server" "Laravel Echo Server" ON)
supportedDockerImages+=("selenium" "Selenium (Automated Testing)" ON)
supportedDockerImages+=("workspace" "Working Environment for Projects" OFF)
supportedDockerImages+=("mysql" "MySQL Database Server" OFF)
supportedDockerImages+=("elasticsearch" "ElasticSearch" OFF)
supportedDockerImages+=("redis" "Redis Server" ON)
supportedDockerImages+=("kibana" "Kibana" OFF)
supportedDockerImages+=("php-worker" "PHP-Worker" OFF)
supportedDockerImages+=("puppeteer" "Puppeteer" OFF)
supportedDockerImages+=("mailhog" "Mail-Hog (Email Testing)" OFF)
supportedDockerImages+=("minio" "Minio" ON)
supportedDockerImages+=("mssql" "Microsoft MSSQL Database Server" OFF)
supportedDockerImages+=("adminer" "Adminer (PHPMyAdmin Alternative)" OFF)
supportedDockerImages+=("meilisearch" "Meilisearch" ON)

selectedDockerImages=$(whiptail --backtitle "$scriptTitle" --title "Docker Images" \
                --checklist "\nSelect the Docker images you wish to install, enable, generate hosts and optionally create aliases for:\n" 23 79 14 \
                "${supportedDockerImages[@]}" 3>&1 1>&2 2>&3)

if [ $? -ne 0 ]; then
    warningMessage "WARNING: User cancelled Docker Image selection. Exiting.";
    exit;
fi

if [[ ${#selectedDockerImages[@]} -eq 0 || $selectedDockerImages = "" ]]; then
    errorMessage "Docker Images required. Please select at least one Docker image.";
    exit;
fi

# ------------------- +++ /Docker: Select Services +++ ------------------ #

# ------------------- +++ Generate: Dynamic Up/Down +++ ------------------ #

## Create: Temporary Output folder
if [ ! -d "tmp-output" ]; then
	mkdir tmp-output
fi


dynamicShellMessage="# This file was dynamically generated by \`visual-install.sh\` BASH Script.";

## Create: restart.sh
restartBashScript="#!/bin/bash \

$dynamicShellMessage

cd $currentLaradockDir; ./down.sh \

cd $currentLaradockDir; ./up.sh";

echo "$restartBashScript" > ./restart.sh;
chmod +x ./restart.sh


## Create: stop.sh
stopBashScript="#!/bin/bash \

$dynamicShellMessage

cd $currentLaradockDir; docker-compose stop";

echo "$stopBashScript" > ./down.sh;
chmod +x ./down.sh


## Create: start.sh
startBashScript="#!/bin/bash \

$dynamicShellMessage

cd $currentLaradockDir; docker-compose up -d";

for service in "${selectedDockerImages[@]}"
do :
	startBashScript="$startBashScript ${service//\"/}"
done


echo "$startBashScript" > ./up.sh;
chmod +x ./up.sh

# ------------------- +++ /Generate: Dynamic Up/Down +++ ------------------ #


# ------------------- +++ OS: Lara-Dock Aliases +++ ------------------ #
addLaradockAlias=$(whiptail --backtitle "$scriptTitle" --title "Add Laradock aliases" --yesno "Would you like to add Laradock aliases to current user?\
\n\n    E.g. $: lara\
\n         $: lara-start\
\n         $: lara-restart\
\n         $: lara-stop\
\n         $: lara-mysql\
\n         $: lara-redis" 15 78 3>&1 1>&2 2>&3 )

if [ addLaradockAlias ]; then
	## Create: File containing aliases, copy to local ~/ folder and tee to bash, resource


	laradockAliases=" \

	alias lara-start='cd $currentLaradockDir; ./up.sh' \

	alias lara-stop='cd $currentLaradockDir; ./stop.sh' \

	alias lara-restart='cd $currentLaradockDir; ./restart.sh' \

	";

	for service in ${selectedDockerImages[@]}
	do :

		if [ "${service//\"/}" != "workspace" ]; then

			laradockAliases="${laradockAliases}"
			laradockAliases="${laradockAliases}alias lara-"
			laradockAliases="${laradockAliases}${service//\"/}";
			laradockAliases="${laradockAliases}='cd $currentLaradockDir; docker-compose exec ";
			laradockAliases="${laradockAliases}${service//\"/} bash'";
			laradockAliases="${laradockAliases}\
			
	";
		else
			laradockAliases="${laradockAliases}"
			laradockAliases="${laradockAliases}alias lara-bash"
			laradockAliases="${laradockAliases}='cd $currentLaradockDir; docker-compose exec --user=laradock workspace bash'";
			laradockAliases="${laradockAliases}\
			
	";

		fi

	done

	echo "$laradockAliases" > ./tmp-output/.lara_aliases;

	
	
	cp ./tmp-output/.lara_aliases /home/$currentUser/.lara_aliases

	if [ -f "/home/$currentUser/.bashrc" ]; then
		prettyOutput "/home/$currentUser/.bashrc file has been found for $currentUser"

		if [ $( cat /home/$currentUser/.bashrc 2>/dev/null | grep -c "lara_aliases" ) -eq 0  ]; then
			
			warningMessage "Lara-Aliases not yet linked...";

			bashRcAppend="\

	";

			bashRcAppend+="if [ -f ~/.lara_aliases ]; then 
		. ~/.lara_aliases
	fi";

			bashRcAppend+="\

	";

			echo "$bashRcAppend" >> /home/$currentUser/.bashrc
			# source /home/$currentUser/.bashrc

		else 
			prettyOutput "Lara-Aliases already linked";
		fi

		prettyServiceOutput "\nPlease run 'source ~/.bashrc' as non-sudo once installation complete to immediately use lara-{service} aliases.\nOtherwise restart terminal to apply changes.\n
		
"

	fi

fi

# ------------------- +++ /OS: Lara-Dock Aliases +++ ------------------ #


prettyOutput "Merging sample hosts to /etc/hosts";

if [ -f hosts.sample ]; then

	while IFS=$'\t' read -r -a hosts
	do
		if [ "${hosts[0]:0:1}" == "#" ]; then
			continue;
		fi

		hostIp="${hosts[0]}"
		hostName="${hosts[1]}"

		prettyOutput "Currently checking hostname: $hostName";

		if [ $( grep -c "$hostName" /etc/hosts ) -ge 1 ]; then
			warningMessage "$hostName already found in /etc/hosts."
		else 
			echo "$hostIp	$hostName" >> /etc/hosts
		fi		

	done < hosts.sample

else
	warningMessage "hosts.sample file not found. Skipping..."
fi


successInfo "Laradock successfully installed!";
prettyOutput "Running \`lara-restart\`...\n
";

./restart.sh


prettyServiceOutput "\nLaradock now running \n";
