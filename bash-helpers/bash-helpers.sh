#!/bin/bash

##########################################################
#                                                        #
#   File: bash-helpers.sh                                #
#   Type: BASH Script                                    #
#   Target OS: Ubuntu 20.04 LTS (Debian)                 #
#   Author: Truc Minh Phan <truc@trucphan.com>           #
#   Description:                                         #
#                                                        #
#   This script is used as a helper file for all         #
#   BASH/Shell Scripts.                                  #
#                                                        #
#   NOTE: +++                                            #
#                                                        #
##########################################################

# ----------------- +++ Output Function +++ ----------------- #
function prettyOutput()
{
    dt=$(date '+%d/%m/%Y %H:%M:%S');
    echo -e "[$dt] -" $1
}
# ----------------- +++ /Output Function +++ ----------------- #



# ----------------- +++ Output Function +++ ----------------- #
function prettyServiceOutput()
{
    serviceColor="\033[1;33m"
    standardColor="\033[0m"
    echo -e $serviceColor$1$standardColor
}
# ----------------- +++ /Output Function +++ ----------------- #


# ----------------- +++ Output Function +++ ----------------- #
function prettyPrompt()
{
    promptBackgroundColor="\033[0;45m"
    promptForegroundColor="\033[1;97m"
    standardForegroundColor="\033[0m"
    standardBackgroundColor="\033[0;49m"
    echo -e "\n$promptBackgroundColor$promptForegroundColor$1$standardBackgroundColor$standardForegroundColor "
}
# ----------------- +++ /Output Function +++ ----------------- #


# ----------------- +++ Output Function +++ ----------------- #
function successInfo()
{
    # promptBackgroundColor="\033[0;42m"
    promptBackgroundColor="\e[30;48;5;22m"
    promptForegroundColor="\033[1;97m"
    standardForegroundColor="\033[0m"
    standardBackgroundColor="\033[0;49m"
    dt=$(date '+%d/%m/%Y %H:%M:%S');

    echo -e "[$dt] - $promptBackgroundColor$promptForegroundColor$1$standardBackgroundColor$standardForegroundColor"
}
# ----------------- +++ /Output Function +++ ----------------- #


# ----------------- +++ Output Function +++ ----------------- #
function errorMessage()
{
    promptBackgroundColor="\e[30;48;5;88m"
    promptForegroundColor="\033[1;97m"
    standardForegroundColor="\033[0m"
    standardBackgroundColor="\033[0;49m"
    dt=$(date '+%d/%m/%Y %H:%M:%S');

    echo -e "[$dt] - $promptBackgroundColor$promptForegroundColor$1$standardBackgroundColor$standardForegroundColor"
}
# ----------------- +++ /Output Function +++ ----------------- #


# ----------------- +++ Output Function +++ ----------------- #
function warningMessage()
{
    promptBackgroundColor="\e[30;48;5;166m"
    # promptForegroundColor="\033[1;97m"
    promptForegroundColor="\e[38;5;256m"
    standardForegroundColor="\033[0m"
    standardBackgroundColor="\033[0;49m"
    dt=$(date '+%d/%m/%Y %H:%M:%S');

    echo -e "[$dt] - $promptBackgroundColor$promptForegroundColor$1$standardBackgroundColor$standardForegroundColor"
}
# ----------------- +++ /Output Function +++ ----------------- #

